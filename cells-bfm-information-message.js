{
  const {
    html,
  } = Polymer;
  /**
    `<cells-bfm-information-message>` Este componente muestra la información necesaria para informar al usuario acerca de sus facturas asociadas a su cuenta Net Cash.

    El componente `<cells-bfm-information-message>` hace uso de los siguientes componentes:

    1.- `<cells-icon-message>`.
    2.- `<cells-lists>`.
    3.- `<cells-st-button>`.

    Example with button:

    ```html
    <cells-bfm-information-message
    title = "title"
    topMessage = "topMessage"
    list = "list"
    downMessage = "downMessage"
    buttonText = "buttonText"
    ></cells-bfm-information-message>
    ```
    Example without button:

    ```html
    <cells-bfm-information-message
    type = "type"
    icon = "icon"
    title = "title"
    topMessage = "topMessage"
    list = "list"
    ></cells-bfm-information-message>
    ```

    ## Styling
    The following custom properties and mixins are available for styling:

    ### Custom Properties
    | Custom Property     | Selector | CSS Property | Value       |
    | ------------------- | -------- | ------------ | ----------- |
    | --cells-fontDefault | :host    | font-family  |  sans-serif |
    ### @apply
    | Mixins                                          | Selector         | Value |
    | ----------------------------------------------- | ---------------- | ----- |
    | --cells-bfm-information-message                 | :host            | {}    |
    | --cells-bfm-information-message-list            | .list            | {}    |
    | --cells-bfm-information-message-helper          | .helper          | {}    |
    | --cells-bfm-information-message-button-icon-msg | .button-icon-msg | {}    |

    * @customElement
    * @polymer
    * @extends {Polymer.Element}
    * @demo demo/index.html
  */
  class CellsBfmInformationMessage extends Polymer.Element {

    static get is() {
      return 'cells-bfm-information-message';
    }

    static get properties() {
      return {
        type: {
          type: String,
          value: ''
        },
        icon: {
          type: String,
          value: ''
        },
        title: {
          type: String,
          value: ''
        },
        topMessage: {
          type: String,
          value: ''
        },
        titleList: {
          type: String,
          value: ''
        },
        list: {
          type: Array,
          value: () => ([])
        },
        downMessage: {
          type: String,
          value: ''
        },
        buttonText: {
          type: String,
          value: ''
        },
        event: {
          type: String,
          value: 'view-bills-event'
        }
      };
    }

    _continue() {
      this.dispatchEvent(new CustomEvent(this.event, {
        bubbles: true,
        composed: true
      }));
    }
  }

  customElements.define(CellsBfmInformationMessage.is, CellsBfmInformationMessage);
}